# Todos

Challenge proposed by @BucDany on discord

## Functions

- [✅] append(value:int|str) -> None 
  - permet d'ajouter un élément à la fin de la liste

- [ ] remove_at(index:int) -> None 
  - retire un élément à l'index donné

- [ ] insert(index:int, value:int|str) -> None 
  - insère un élément à l'index donné

- [✅] contains(value:int|str) -> bool 
  - vérifie que value est présent dans la liste

- [✅] index_of(value:int|str) -> int 
  - renvoie l'index de la première valeur rencontrée, -1 si value n'est pas présente dans la liste

- [✅] nth(index)-> int|str 
  - renvoie la valeur à l'index donné

- [✅] len(llist)-> int Et 
  - renvoie le nombre d'éléments contenus dans la liste chaînée, passée en argument

- [✅] print(llist)-> str 
  - affiche la chaîne de caractères pour représenter le contenu de la liste

- [✅] is_unique()-> bool
  - vérifie que la liste ne contient pas de doublons

- [ ] reversed(llist)-> None 
  - inverse la liste passée en argument