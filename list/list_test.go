package list

import (
	"fmt"
	"reflect"
	"testing"
)

// Deprecated
func TestShouldReturnNewEmptyList(t *testing.T) {
	got := NewLinkedList()

	expect := &LinkedList{Size: 0, Head: nil}
	if !reflect.DeepEqual(expect, got) {
		t.Errorf("expected %v but got %v", expect, got)
	}
}

func TestShouldReturnNilWhenAccessingFirstElementWithEmptyList(t *testing.T) {
	l := NewLinkedList()

	elem := l.First()
	if elem != nil {
		t.Errorf("expected nil but got %v", elem)
	}
}

func TestShouldReturnElementAtNthIndex(t *testing.T) {
	testCases := []struct {
		GivenList  LinkedList
		GivenIndex uint
		Expect     *Node
	}{
		{
			GivenList: LinkedList{
				Size: 1,
				Head: &Node{Value: 1},
			},
			GivenIndex: 0,
			Expect:     &Node{Value: 1},
		},
		{
			GivenList: LinkedList{
				Size: 2,
				Head: &Node{
					Value: 1,
					Next:  &Node{Value: 2},
				},
			},
			GivenIndex: 1,
			Expect:     &Node{Value: 2},
		},
		{
			GivenList: LinkedList{
				Size: 3,
				Head: &Node{
					Value: 1,
					Next: &Node{
						Value: 2,
						Next:  &Node{Value: 3},
					},
				},
			},
			GivenIndex: 2,
			Expect:     &Node{Value: 3},
		},
	}

	for _, testCase := range testCases {
		testName := fmt.Sprintf(
			"accessing element %v should return %v",
			testCase.GivenIndex,
			testCase.Expect,
		)
		t.Run(testName, func(t *testing.T) {
			got, _ := testCase.GivenList.Nth(testCase.GivenIndex)

			if !reflect.DeepEqual(got, testCase.Expect) {
				t.Errorf("expected %v but got %v", testCase.Expect, got)
			}
		})
	}
}

func TestShouldReturnErrorWhenAccessingElementAtInvalidIndex(t *testing.T) {
	given := NewLinkedList()

	_, err := given.Nth(10)

	if err == nil {
		t.Error("expected an error but got nil")
	}
}

func TestShouldIncrementSizeByOneWhenAppendingOneElement(t *testing.T) {
	given := NewLinkedList()

	given.Append(1)

	expect := uint(1)

	got := given.Len()
	if got != expect {
		t.Errorf("expected %v but got %v", expect, given)
	}
}

func TestShouldIncrementSizeByTwoWhenAppendingTwoElements(t *testing.T) {
	given := NewLinkedList()

	given.Append(1)
	given.Append(2)

	expect := uint(2)

	got := given.Len()
	if got != expect {
		t.Errorf("expected %v but got %v", expect, given)
	}
}

func TestShouldReturnFirstElementEqualToFirstElementAppended(t *testing.T) {
	// Given
	given := NewLinkedList()

	//	When
	given.Append(1)

	//	Then
	expect := &Node{Value: 1}
	if !reflect.DeepEqual(given.Head, expect) {
		t.Errorf("expected %v but got %v", expect, given.Head)
	}
}

func TestShouldAppend2Elements(t *testing.T) {
	// Given
	given := NewLinkedList()

	//	When
	given.Append(1)
	given.Append(2)

	//	Then
	expect := &Node{
		Value: 1,
		Next:  &Node{Value: 2},
	}
	if !reflect.DeepEqual(given.Head, expect) {
		t.Errorf("expected %v but got %v", expect, given.Head)
	}
}

func TestShouldAppend3Elements(t *testing.T) {
	// Given
	given := NewLinkedList()

	//	When
	given.Append(1)
	given.Append(2)
	given.Append(3)

	//	Then
	expect := &Node{
		Value: 1,
		Next: &Node{
			Value: 2,
			Next:  &Node{Value: 3},
		},
	}
	if !reflect.DeepEqual(given.Head, expect) {
		t.Errorf("expected %v but got %v", expect, given.Head)
	}
}

func TestShouldReturnLastElement(t *testing.T) {
	testCases := []struct {
		Given  *LinkedList
		Expect *Node
	}{
		{Given: NewLinkedList(), Expect: nil},
		{
			Given: &LinkedList{
				Size: 1,
				Head: &Node{Value: 1},
				Tail: &Node{Value: 1},
			},
			Expect: &Node{Value: 1},
		},
		{
			Given: &LinkedList{
				Size: 2,
				Head: &Node{
					Value: 1,
					Next:  &Node{Value: 2},
				},
				Tail: &Node{Value: 2},
			},
			Expect: &Node{Value: 2},
		},
	}

	for _, testCase := range testCases {
		testName := fmt.Sprintf("should return %v", testCase.Expect)

		t.Run(testName, func(t *testing.T) {
			got := testCase.Given.Last()
			if !reflect.DeepEqual(got, testCase.Expect) {
				t.Errorf("expected %v but got %v", testCase.Expect, got)
			}
		})
	}
}

func TestShouldReturnNegativeValueWhenSearchingElementOnEmptyList(t *testing.T) {
	given := NewLinkedList()

	got := given.IndexOf(1)
	expect := -1

	if got != expect {
		t.Errorf("expected %v but got %v", expect, got)
	}
}

func TestShouldReturnNegativeValueWhenSearchingElementThatDoesntExists(t *testing.T) {
	given := &LinkedList{
		Size: 2,
		Head: &Node{
			Value: 1,
			Next:  &Node{Value: 2},
		},
	}

	got := given.IndexOf(10)
	expect := -1

	if got != expect {
		t.Errorf("expected %v but got %v", expect, got)
	}
}

func TestShouldReturnIndex0WhenSearchingElementThatAppearsFirstInList(t *testing.T) {
	given := &LinkedList{
		Size: 2,
		Head: &Node{
			Value: 1,
			Next:  &Node{Value: 2},
		},
	}

	got := given.IndexOf(1)
	expect := 0

	if got != expect {
		t.Errorf("expected %v but got %v", expect, got)
	}
}

func TestShouldReturnIndex1WhenSearchingElementThatAppearsSecondInList(t *testing.T) {
	given := &LinkedList{
		Size: 2,
		Head: &Node{
			Value: 1,
			Next:  &Node{Value: 2},
		},
	}

	got := given.IndexOf(2)
	expect := 1

	if got != expect {
		t.Errorf("expected %v but got %v", expect, got)
	}
}

func TestShouldReturnIfListIsUnique(t *testing.T) {
	testCases := []struct {
		Given  *LinkedList
		Expect bool
	}{
		{Given: NewLinkedList(), Expect: true},
		{
			Given: &LinkedList{
				Size: 1,
				Tail: NewNode(1),
				Head: NewNode(1),
			},
			Expect: true,
		},
		{
			Given: &LinkedList{
				Size: 2,
				Head: &Node{
					Value: 2,
					Next:  NewNode(2),
				},
				Tail: NewNode(2),
			},
			Expect: false,
		},
	}

	for _, testCase := range testCases {
		testName := fmt.Sprintf("should return %v", testCase.Expect)

		t.Run(testName, func(t *testing.T) {
			got := testCase.Given.IsUnique()

			if got != testCase.Expect {
				t.Errorf("expected %v but got %v", testCase.Expect, got)
			}
		})
	}
}

func TestShouldReturnListRepr(t *testing.T) {
	testCases := []struct {
		Given  *LinkedList
		Expect string
	}{
		{Given: NewLinkedList(), Expect: "[]"},
		{Given: From(1), Expect: "[1]"},
		{Given: From(1, 2), Expect: "[1 2]"},
	}

	for _, testCase := range testCases {
		testName := fmt.Sprintf("should return %v", testCase.Expect)
		t.Run(testName, func(t *testing.T) {
			got := testCase.Given.String()
			if got != testCase.Expect {
				t.Errorf("expected %v but fot %v", testCase.Expect, got)
			}
		})
	}
}

func TestContains(t *testing.T) {
	testCases := []struct {
		GivenList    *LinkedList
		GivenElement int
		Expect       bool
	}{
		{GivenList: NewLinkedList(), GivenElement: 1, Expect: false},
		{GivenList: From(1, 2), GivenElement: 3, Expect: false},
		{GivenList: From(1, 2), GivenElement: 2, Expect: true},
	}

	for _, testCase := range testCases {
		testName := fmt.Sprintf(
			"should return %v when %v is contained in list %v",
			testCase.Expect,
			testCase.GivenElement,
			testCase.GivenList,
		)
		t.Run(testName, func(t *testing.T) {
			got := testCase.GivenList.Contains(testCase.GivenElement)
			if got != testCase.Expect {
				t.Errorf("expected %v but got %v", testCase.Expect, got)
			}
		})
	}
}

func TestShouldRemoveAtIndex(t *testing.T) {
	testCases := []struct {
		GivenList  *LinkedList
		GivenIndex uint
		Expect     *LinkedList
	}{
		{GivenList: From(1, 2, 3), GivenIndex: 0, Expect: From(2, 3)},
		{GivenList: From(1, 2, 3), GivenIndex: 1, Expect: From(1, 3)},
		{GivenList: From(1, 2, 3), GivenIndex: 2, Expect: From(1, 2)},
		{GivenList: From(1), GivenIndex: 0, Expect: NewLinkedList()},
	}

	for _, testCase := range testCases {
		testName := fmt.Sprintf(
			"given %v should return %v when removing at index %v",
			testCase.GivenList,
			testCase.Expect,
			testCase.GivenIndex,
		)
		t.Run(testName, func(t *testing.T) {
			_ = testCase.GivenList.RemoveAt(testCase.GivenIndex)
			if !reflect.DeepEqual(testCase.GivenList, testCase.Expect) {
				t.Errorf("expected %v but got %v", *testCase.Expect, *testCase.GivenList)
			}
		})
	}
}

func TestShouldReturnErrWhenRemoveAtOutOfRangeIndex(t *testing.T) {
	given := NewLinkedList()

	err := given.RemoveAt(1)

	if err == nil {
		t.Error("expected an error but got nil")
	}
}
