package list

import (
	"fmt"
	"strings"
)

type LinkedList struct {
	Size uint
	Head *Node
	Tail *Node
}

func NewLinkedList() *LinkedList {
	return &LinkedList{Size: 0}
}

func From(elems ...int) *LinkedList {
	l := NewLinkedList()

	for _, elem := range elems {
		l.Append(elem)
	}
	return l
}

func (l *LinkedList) Len() uint {
	return l.Size
}

func (l *LinkedList) First() *Node {
	return l.Head
}

func (l *LinkedList) Last() *Node {
	return l.Tail
}

func (l *LinkedList) Nth(index uint) (*Node, error) {
	if index >= l.Size {
		return nil, fmt.Errorf("index out of range")
	}
	currIndex := uint(0)
	currNode := l.Head

	for currIndex < index && currNode != nil {
		currNode = currNode.Next
		currIndex += 1
	}
	return currNode, nil
}

func (l *LinkedList) Append(value int) {
	l.Size += 1

	if l.Head == nil {
		l.Head = NewNode(value)
		l.Tail = l.Head
	} else {
		newNode := NewNode(value)
		l.Last().Next = newNode
		l.Tail = newNode
	}
}

func (l *LinkedList) IndexOf(value int) int {
	if l.Head == nil {
		return -1
	}
	currNode := l.Head
	currIndex := 0

	for currNode != nil {
		if currNode.Value == value {
			return currIndex
		}
		currNode = currNode.Next
		currIndex += 1
	}
	return -1
}

func (l *LinkedList) IsUnique() bool {
	if l.Head == nil {
		return true
	}
	visited := make(map[int]bool)
	currNode := l.Head

	for currNode != nil {
		if _, ok := visited[currNode.Value]; ok {
			return false
		}
		visited[currNode.Value] = true
		currNode = currNode.Next
	}
	return true
}

func (l *LinkedList) String() string {
	repr := strings.Trim(
		strings.Join(
			strings.Fields(fmt.Sprint(l.ToSlice())),
			" ",
		),
		"[]",
	)
	return fmt.Sprintf("[%s]", repr)
}

func (l *LinkedList) ToSlice() []int {
	elems := make([]int, 0, l.Size)

	l.ForEach(func(elem int) {
		elems = append(elems, elem)
	})
	return elems
}

func (l *LinkedList) ForEach(f func(int)) {
	currNode := l.Head

	for currNode != nil {
		f(currNode.Value)
		currNode = currNode.Next
	}
}

func (l *LinkedList) RemoveAt(index uint) error {
	if index >= l.Size {
		return fmt.Errorf("index out of range")
	}
	if index == 0 {
		if l.Size == 1 {
			l.Head = nil
			l.Tail = nil
		} else {
			l.Head = l.Head.Next
		}
	} else {
		prevNode, _ := l.Nth(index - 1)
		if prevNode.Next == l.Tail {
			l.Tail = prevNode
			prevNode.Next = nil
		} else {
			prevNode.Next = prevNode.Next.Next
		}
	}
	l.Size -= 1
	return nil
}

func (l *LinkedList) Contains(elem int) bool {
	return l.IndexOf(elem) != -1
}

func (l *LinkedList) Print() {
	fmt.Println(l.String())
}
