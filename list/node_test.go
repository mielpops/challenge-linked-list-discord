package list

import (
	"reflect"
	"testing"
)

func TestShouldReturnNewNodeWithValue(t *testing.T) {
	given := NewNode(0)

	expect := &Node{Value: 0, Next: nil}
	if !reflect.DeepEqual(given, expect) {
		t.Errorf("expected %v but got %v", given, expect)
	}
}
