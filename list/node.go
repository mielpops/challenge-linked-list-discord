package list

import "fmt"

type Node struct {
	Value int
	Next  *Node
}

func NewNode(value int) *Node {
	return &Node{
		Value: value,
		Next:  nil,
	}
}

func (n Node) String() string {
	return fmt.Sprintf("%v", n.Value)
}
