package tests

import (
	"linkedlist/list"
	"reflect"
	"testing"
)

func TestShouldIterateThroughEachElementOfTheList(t *testing.T) {
	given := list.From(1, 2, 3, 4)
	var got []int

	given.ForEach(func(elem int) {
		got = append(got, elem)
	})

	expect := []int{1, 2, 3, 4}
	if reflect.DeepEqual(given, expect) {
		t.Errorf("expected %v but got %v", expect, got)
	}
}
